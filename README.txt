CONTENTS OF THIS FILE
---------------------

Introduction
Installation
FAQ
Maintainers

INTRODUCTION
------------

Build a buzzing community using our responsive, customizable chat module with multiple chat rooms. Sign up for free and get going.

INSTALLATION
------------

1. Log in to your Drupal Dashboard, go to the 'Extend' tab and click the 'contributed modules' link.
2. Search for Chatwee and install it from Drupal directory.
3. Enable Chatwee in the modules list and click 'Configure'.
4. Log in to your Chatwee Dashboard at https://chatwee.com/ or sign op for free if you don't have account yet.
5. Copy the installation code and paste it into the box in Chatwee settings. Click 'Save configuration'.
6. Done! Feel free to explore the mod's customization options in your Chatwee Dashboard.

FAQ
—

Customization

Q: I need Chatwee to really match the style of my site. Is that possible?
A: While generally aiming for modern design, we still wanted to leave some room for adjustments. Indeed, our live chat widget allows you to make a number of changes in order to create an even better fit. What’s more, we’re open to further customization as a part of an Enterprise solution, in case you’d like the chat to have a look unique to your site.

Q: How much customization can I do with Chatwee?
A: From determining the size of the chat window, choosing the language, deciding upon privacy settings (including log in methods), to assigning moderators and banning rogue users, plus a bunch of other minor tweaks. Conveniently manage the chat directly from your Dashboard.

Q: What if I like Chatwee overall but it’s missing some features I find crucial?
A: We appreciate the fact that the needs of our users are as diverse as the sites they run. Chatwee as it is already caters to the demands of a wide range of online communities, however, if there are features you’d like to see, that aren’t available in the standard, please contact us and we’ll discuss the development of an Enterprise solution for you.

Billing

Q: How much does Chatwee cost?
A: We offer a Free-forever plan to whomever it suits. If, however, you’d like to see your online community grow, you might want to consider an upgrade.

Q: What methods of payment do you accept?
A: We accept credit cards and PayPal as the payment methods. In case you’d like to switch from one method to the other, or your credit card becomes invalid, please cancel your current subscription and subscribe again with the new details.

Q: How flexible is my subscription?
A: Your Chatwee subscription is very flexible. You can cancel, upgrade, or downgrade anytime you want. There are no hidden fees whatsoever. More information regarding billing is available in the Subscription tab of your Dashboard.

Other FAQ

Q: I followed the installation instructions but Chatwee isn’t showing on my site. What can be wrong?
A: After you paste the installation code into your website and save all the changes, please check if it’s actually there. Open your site in any browser, right-click the mouse anywhere on the page where the chat is supposed to display and view the source code. Is our script there?

Q: Can I merge Chatwee with my site’s log in system?
A: Chatwee offer what we call Single Sign-on, which upon implementation, allows users who log in to their website accounts to automatically pop up in the chat as well. The implementation instructions can be found here. Please keep in mind that this feature is only available with paid plans, but you can request a free trial to test it anytime.

MAINTAINERS
-----------
Current maintainers:

Wojciech Majerski (paul_q) - https://drupal.org/user/2811031

Please visit https://chatwee.com/ to find out more about Chatwee.